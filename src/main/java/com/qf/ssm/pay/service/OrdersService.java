package com.qf.ssm.pay.service;

import com.qf.ssm.pay.pojo.Orders;

import java.util.List;

public interface OrdersService {
    void addOrder(Orders orders);

    List<Orders> getNotNotifyOrderList();

    void updateStatus(Orders orders);
}
