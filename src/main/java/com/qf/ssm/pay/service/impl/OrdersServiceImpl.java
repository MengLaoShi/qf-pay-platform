package com.qf.ssm.pay.service.impl;

import com.qf.ssm.pay.dao.OrdersDAO;
import com.qf.ssm.pay.pojo.Orders;
import com.qf.ssm.pay.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersDAO ordersDAO;

    @Override
    public void addOrder(Orders orders) {
        ordersDAO.addOrder(orders);
    }

    @Override
    public List<Orders> getNotNotifyOrderList() {
        return ordersDAO.getNotNotifyOrderList();
    }

    @Override
    public void updateStatus(Orders orders) {
        ordersDAO.updateStatus(orders);
    }
}
