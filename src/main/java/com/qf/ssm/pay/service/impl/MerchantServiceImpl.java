package com.qf.ssm.pay.service.impl;

import com.qf.ssm.pay.dao.MerchantDAO;
import com.qf.ssm.pay.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    private MerchantDAO merchantDAO;

    @Override
    public String getScretKeyById(Integer id) {
        return merchantDAO.getScretKeyById(id);
    }
}
