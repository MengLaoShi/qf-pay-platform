package com.qf.ssm.pay.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class Orders {

    private String orderId;//商家平台订单号
    private Integer merchantId;//商户号
    private Date createdDate;//下单时间
    private String tradeNum;//千锋平台订单号
    @JsonIgnore
    private Integer payStatus;//支付状态，0未支付 1成功 -1失败
    private String notifyURL;//支付回调地址
    private String otherParam;//商户的其它参数，原样返回
    @JsonIgnore
    private Integer notifyTimes;//已经回调商户的次数
    private Integer price;//支付金额

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(String tradeNum) {
        this.tradeNum = tradeNum;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getNotifyURL() {
        return notifyURL;
    }

    public void setNotifyURL(String notifyURL) {
        this.notifyURL = notifyURL;
    }

    public String getOtherParam() {
        return otherParam;
    }

    public void setOtherParam(String otherParam) {
        this.otherParam = otherParam;
    }

    public Integer getNotifyTimes() {
        return notifyTimes;
    }

    public void setNotifyTimes(Integer notifyTimes) {
        this.notifyTimes = notifyTimes;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderId='" + orderId + '\'' +
                ", merchantId=" + merchantId +
                ", createdDate=" + createdDate +
                ", tradeNum='" + tradeNum + '\'' +
                ", payStatus=" + payStatus +
                ", notifyURL='" + notifyURL + '\'' +
                ", otherParam='" + otherParam + '\'' +
                ", notifyTimes=" + notifyTimes +
                '}';
    }
}
