package com.qf.ssm.pay.dao;

import com.qf.ssm.pay.pojo.Orders;

import java.util.List;

public interface OrdersDAO {

    void addOrder(Orders orders);

    List<Orders> getNotNotifyOrderList();

    void updateStatus(Orders orders);

}
